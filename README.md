# Ansible

## Provisioning

```bash
ansible-playbook --vault-password-file ./vault -i hosts.yml email_server_config.yml -K
```

## Secrets

to encrypt the string ‘foobar’ using the only password stored in ‘a_password_file’ and name the variable ‘the_secret’:

```bash
ansible-vault encrypt_string --vault-password-file ./vault 'foobar' --name 'the_secret'
```

- <https://docs.ansible.com/ansible/2.9/user_guide/vault.html#variable-level-encryption>

### Debugging

- <https://wiki2.dovecot.org/TestInstallation>

```bash
# this checks whether the service is running
telnet hostname.com 10025
# this checks whether dovecot is running
telnet hostname.com 10143

# this checks whether port forwarding is working
telnet hostname.com 25
# this checks whether port forwarding is working for dovecot
telnet hostname.com 143


sudo certbot certonly -n --test-cert --standalone -d hostname.com --debug-challenges -v --agree-tos --email admin@hostname.com
sudo certbot certonly --standalone -d hostname.com --debug-challenges -v --agree-tos --email admin@hostname.com

sudo ufw disable
sudo ufw enable
```

- [collection of shell scripts created to test a mailserver's setup](git@github.com:tinned-software/mailserver-test.git)

### Migrating from old server

```bash
docker run gilleslamiral/imapsync imapsync --host1 old-domain.com --user1 test1 --password1 'secret1' \
                                           --host2 new-domain.com --user2 test2 --password2 'secret2'
```

## Development

Install current Vagrant: <https://www.vagrantup.com/downloads>

On Jammy the deb Package must be used directly: <https://stackoverflow.com/questions/71987581/openssl-3-0-error-when-booting-vagrantbox>

```bash
sudo apt install ruby-libvirt qemu libvirt-daemon-system libvirt-clients ebtables dnsmasq-base libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev libguestfs-tools

vagrant plugin install vagrant-mutate
vagrant plugin install vagrant-dns
vagrant dns --install
vagrant plugin install vagrant-scp

brew install ansible
ansible-galaxy install -r requirements.yml
```

### DNS

Follow the instructions on <https://github.com/BerlinVagrant/vagrant-dns#usage>

### Updating dependencies

```bash
vagrant plugin update
```

### Virtualbox

```bash
sudo apt install virtualbox virtualbox-ext-pack
```

### Libvirt

```bash
sudo apt install vagrant-libvirt

or

vagrant plugin install vagrant-libvirt

# either use this image
vagrant box add --provider virtualbox barbosaaob/ubuntu2204

# or this if it is finally released
vagrant box add --provider libvirt generic/ubuntu2204
```

### Vagrant

You can run provision the VM with

```bash
vagrant up --provision
```

And you can do

```bash
vagrant halt; vagrant destroy;
```

If you want to start fresh.

### Navigating on the machine

You can switch to app user context where the containers are running with the alias `app`.
As `app` user, you have [more handy aliases](roles/base_setup/templates/app_bash_aliases.sh.j2) that you might want to use.
So you can type `services` to see the state of all containers and `podmanps` gives a shorter formatted output of `podman ps`.

To see container logs, you can use `${name}_log` so for Authelia logs, you can type `authelia_log` and the service log will be shown with the alias `authelia_service_log`.

## Obstacles

### Vagrant failed to install the requested plugin because it depends on a library which is not currently installed on this system. The following library is required by the 'vagrant-libvirt' plugin

This could be caused by [`brew`](https://github.com/vagrant-libvirt/vagrant-libvirt/issues/1498#issuecomment-1133594122):

Do this:

```bash
brew remove --ignore-dependencies pkg-config
```
