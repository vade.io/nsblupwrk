require 'socket'

# rubocop:disable Metrics/BlockLength

Vagrant.require_version '>= 1.8.0'

# you might want to lower the resources if your machine only has few memory or CPU
MEMORY = 5072
CPUS = 3

# Docker variant is not working yet
PROVIDER = :libvirt # docker | libvirt | virtualbox
IP = '192.168.56.45'.freeze

# Port 80 is only needed for subdomain testing with sslip.io — otherwise feel free to use an unprivileged port
# This is the port that will be used on localhost. So if 192.168.122.1 is your local IP you should be able to
# access the VM with this URL:
# http://192.168.122.1.sslip.io/
# Read about the details at: https://sslip.io/
LOCALHOST_HTTP_PORT = 8080

LOCALHOST_HOSTNAME = 'local.test'.freeze

LOCALHOST_IP = Socket.ip_address_list
                     .reject(&:ipv4_loopback?)
                     .reject(&:ipv6_loopback?)
                     .reject(&:ipv4_private?)
                     .first&.ip_address

BASE_DOMAIN = LOCALHOST_HOSTNAME
# BASE_DOMAIN = "#{LOCALHOST_IP}.sslip.io".freeze

Vagrant.configure(2) do |config|
  case PROVIDER
  when :docker
    config.ssh.username = 'vagrant'
    config.ssh.password = 'vagrant'

    # https://www.vagrantup.com/docs/providers/docker
    config.vm.provider 'docker' do |d|
      # d.image = 'ubuntu:22.04'
      d.build_dir = '.'
      d.has_ssh = true
      d.remains_running = true
      # https://github.com/hashicorp/vagrant/blob/main/website/content/docs/providers/docker/configuration.mdx#optional
      # needed for systemd commands
      d.create_args = ['--privileged']
      # d.create_args = ['--mount type=tmpfs,destination=/tmp''--mount type=tmpfs,destination=/run', '-v /sys/fs/cgroup:/sys/fs/cgroup:ro', '--name ansible-ubuntu']
    end
  when :libvirt
    # https://app.vagrantup.com/barbosaaob/boxes/ubuntu2204
    config.vm.box = 'barbosaaob/ubuntu2204'
    config.vm.synced_folder '.', '/vagrant', disabled: true
    config.vm.provider 'libvirt' do |v|
      v.memory = MEMORY
      v.cpus = CPUS
    end
  when :virtualbox
    # https://app.vagrantup.com/ubuntu/boxes/jammy64
    config.vm.box = 'ubuntu/jammy64'
    config.vm.synced_folder '.', '/vagrant', disabled: true
    config.vm.provider 'virtualbox' do |vm|
      # vm.gui = true
      vm.memory = MEMORY
      vm.cpus = CPUS
    end
  else
    raise "Unknown provider #{PROVIDER}"
  end

  config.vm.network 'private_network', ip: IP
  # https://www.vagrantup.com/docs/networking/forwarded_ports
  config.vm.network 'forwarded_port', guest: 8080, host: LOCALHOST_HTTP_PORT
  config.dns.tld = 'test'
  config.dns.patterns = [/^(\w+\.)*#{LOCALHOST_HOSTNAME.gsub('.', '\.')}$/]
  config.vm.hostname = 'vagrantvm'

  config.vm.provision 'ansible' do |ansible|
    ansible.verbose = 'v' # vvvv
    ansible.playbook = 'email_server_config_vm.yml' # 'vm_config.yml'
    ansible.compatibility_mode = '2.0'

    ansible.extra_vars = {
      ansible_sudo_pass: 'vagrant',
      base_domain: BASE_DOMAIN
    }

    if provisioning?
      puts "\n\nYou should be able to visit \e[4m\e[1;34m" \
           "http://#{BASE_DOMAIN}" \
           "/\e[0m in your browser after Ansible ran.\n\n"
    end
  end
end


def provisioning?
  (ARGV & %w[reload provision --provision]).any?
end
