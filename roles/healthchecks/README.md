# Healthchecks

## Authentication

- no official OIDC integration yet: <https://github.com/healthchecks/healthchecks/discussions/752#discussioncomment-4420127>
- no official LDAP integration yet: <https://github.com/healthchecks/healthchecks/discussions/751>

### Header Authentication

- <https://github.com/healthchecks/healthchecks#external-authentication>
- <https://github.com/healthchecks/healthchecks/pull/457>

## TODO

- S3 integration: <https://github.com/healthchecks/healthchecks#external-object-storage>
