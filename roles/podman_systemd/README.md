# podman_systemd

A podman configuration role that will install a service as a systemd unit for a user.

## Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

## Role Variables

Privilege port start

```shell
ip_unprivileged_port_start: 25

```

Pod network range:

```yaml
default_podman_network:
    name: "vm_net"
    internal: false
    ip_range: "10.89.0.0/25"
    subnet: "10.89.0.0/24"
    gateway: "10.89.0.1"
    recreate: false
    state: present

```
