# GitLab

- <https://www.reddit.com/r/selfhosted/comments/11sqpfn/selfhosting_gitlab_how_to_use_plain_http_between/****>
- <https://docs.gitlab.com/ee/security/reset_user_password.html#reset-your-root-password>

- <https://docs.gitlab.com/omnibus/docker/#installation>
- configure it to use external Postgres and Redis: <https://docs.gitlab.com/charts/advanced/>
- [configure jemalloc](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1296): `LD_PRELOAD=/usr/lib/libjemalloc.so`
- hardening: <https://about.gitlab.com/blog/2023/05/23/how-to-harden-your-self-managed-gitlab-instance/>
- configure GitLab via `GITLAB_OMNIBUS_CONFIG` env variable: <https://docs.gitlab.com/ee/install/docker.html#pre-configure-docker-container>
  - [ ] including two preconfigured GitLab runner — this might be useful:
    - <https://gitlab.com/qontainers/pipglr/-/tree/main>
    - <https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands-beta>
    - <https://der-jd.de/blog/2021/04/16/Using-podman-instead-of-docker-for-your-gitlab-runner-as-docker-executor/>
    - <https://github.com/jonasbb/podman-gitlab-runner>
  - [ ] Authelia authentication: <https://www.authelia.com/integration/openid-connect/gitlab/tml>
  - [ ] activate Packages ( <https://docs.gitlab.com/ee/administration/packages/index.html#enabling-the-packages-feature> )
  - [ ] activate Container Registry ( <https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-domain-configuration> )
  - [ ] configure Prometheus monitoring ( <https://docs.gitlab.com/ee/administration/monitoring/prometheus/> )
  - [ ] configure Mattermost integration ( <https://docs.gitlab.com/omnibus/gitlab-mattermost/#getting-started> )
  - [ ] use and test a configurable unprivileged Port for git communication (this must be publicly exposed)
  - [ ] Disable sign-up without UI <https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/2837#note_703706180>
  - [ ] disable usage statistics: <https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#enable-or-disable-usage-statistics>
  - [ ] integrate fail2ban filter <https://gitlab.com/MiGoller/gitlab-fail2ban-filter>
  - this could be useful as an inspiration: <https://github.com/search?q=%22podman_container%22+gitlab+language%3AYAML&type=code>
- Backup and restore is documented here: <https://docs.gitlab.com/ee/raketasks/backup_restore.html#back-up-gitlab>
- [ ] Limit memory to 2GB: <https://docs.ansible.com/ansible/latest/collections/containers/podman/podman_container_module.html#parameter-memory>
- Setup Memory tweaks: <https://docs.gitlab.com/omnibus/settings/memory_constrained_envs.html>
  - [ ] disable Puma Clustered mode
  - [ ] `sidekiq['max_concurrency'] = 2`
  - [ ] `unicorn['worker_processes'] = 2`
  - [ ] `postgresql['shared_buffers'] = "256MB"`
  - [ ] Optimize Gitaly
  - [ ] monitoring still has to be kept activated
  - [ ] use the "`Configure how GitLab handles memory`" tweak
- this playbook has some similarities and supports setting these tweaks but it's currently using Docker: <https://github.com/yabusygin/ansible-role-gitlab/blob/master/tasks/main.yml>

## Error

```log
FATAL: Stacktrace dumped to /opt/gitlab/embedded/cookbooks/cache/cinc-stacktrace.out
FATAL: ---------------------------------------------------------------------------------------
FATAL: PLEASE PROVIDE THE CONTENTS OF THE stacktrace.out FILE (above) IF YOU FILE A BUG REPORT
FATAL: ---------------------------------------------------------------------------------------
FATAL: Mixlib::ShellOut::ShellCommandFailed: rails_migration[gitlab-rails] (gitlab::database_migrations line 51) had an error: Mixlib::ShellOut::ShellCommandFailed: bash_hide_env[migrate gitlab-rails database] (gitlab::database_migrations line 18) had an error: Mixlib::ShellOut::ShellCommandFailed: Expected process to exit with [0], but received '137'
---- Begin output of "bash"  ----
STDOUT:
STDERR:
---- End output of "bash"  ----
Ran "bash"  returned 137
```
