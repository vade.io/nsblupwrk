#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# If we have starship use it. Otherwise use prezto
if [[ -s "/home/linuxbrew/.linuxbrew/bin/starship" ]]; then
  eval "$(/home/linuxbrew/.linuxbrew/bin/starship init zsh)"
  HISTFILE=~/.zsh_history
  HISTSIZE=40000
  export SAVEHIST=40000
  setopt appendhistory
elif [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  # Source Prezto.
  # shellcheck disable=SC1091
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

export HOMEBREW_NO_ANALYTICS=1
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
GPG_TTY=$(tty)
export GPG_TTY
export EDITOR=vim

if [[ -s "${ZDOTDIR:-$HOME}/.bash_aliases" ]]; then
  # shellcheck disable=SC1091
  source "${ZDOTDIR:-$HOME}/.bash_aliases"
fi

# Enable Ctrl+arrow key bindings for word jumping
bindkey '^[[1;5C' forward-word     # Ctrl+right arrow
bindkey '^[[1;5D' backward-word    # Ctrl+left arrow

# https://stackoverflow.com/questions/71555133/get-oh-my-zsh-history-behaviour-in-prezto
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
