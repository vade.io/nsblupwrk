# email server

## TODO

- auto SPAM learning: <https://github.com/docker-mailserver/docker-mailserver/issues/3099>
- get real IPs:
  - <https://docker-mailserver.github.io/docker-mailserver/edge/config/advanced/podman/>
  - <https://www.redhat.com/sysadmin/container-ip-address-podman>
- <https://docker-mailserver.github.io/docker-mailserver/edge/config/security/rspamd/>
- <https://github.com/docker-mailserver/docker-mailserver/issues/3099#issuecomment-1447217847>
- DMARC!
- Backup: <https://docker-mailserver.github.io/docker-mailserver/edge/faq/#what-about-backups>
- integrate abusix: <https://github.com/docker-mailserver/docker-mailserver/issues/3003>
- use external Redis server:
  - <https://github.com/docker-mailserver/docker-mailserver/pull/3132/files>
  - persist: <https://github.com/docker-mailserver/docker-mailserver/issues/3138>
- encryption: <https://docker-mailserver.github.io/docker-mailserver/edge/config/security/mail_crypt/>
- <https://serverfault.com/questions/764641/spamassassin-dovecot-and-postfix-move-spam-to-folder>
- DKIM: <https://docker-mailserver.github.io/docker-mailserver/edge/usage/#dns-dkim>

## Bugs

```log
Error: command required for rootless mode with multiple IDs: exec: "newuidmap": executable file not found in $PATH
```

___

```log
could not find slirp4netns, the network namespace can't be configured: exec: \\\"slirp4netns\\\": executable file not found in $PATH\
```

## Max Mail Size

```log
- msg INBOX/86185 {15294726} could not append ( Subject:[=?utf-8?Q?Tabla_conjugaci=C3=B3n_verbos?=], Date:["19-Sep-2021 15:22:34 +0000"], Size:[15294726], Flags:[\Seen NonJunk] ) to folder INBOX: Error sending 'APPEND INBOX (\Seen NonJunk) "19-Sep-2021 15:22:34 +0000" {15294726}': 781 NO [LIMIT] Mail size is larger than the maximum size allowed by server configuration (0.001 + 0.000 secs).
```

- <https://doc.dovecot.org/configuration_manual/quota/#maximum-saved-mail-size>
