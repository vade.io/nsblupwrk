---
# https://github.com/docker-mailserver/docker-mailserver
# https://raw.githubusercontent.com/docker-mailserver/docker-mailserver/master/docker-compose.yml
# https://docker-mailserver.github.io/docker-mailserver/edge/config/advanced/podman/#installation-in-rootless-mode

- include_tasks: roles/container_base/tasks/set_uid_and_gid.yml

# create config dir
- name: "📁 Create {{ mailserver_config_dir }}"
  ansible.builtin.file:
    path: "{{ mailserver_config_dir }}"
    state: directory
  become_user: "{{ app_user }}"

# this file will be copied to /etc/dovecot/local.conf within the container
# https://github.com/docker-mailserver/docker-mailserver/blob/f35b60042f44ada36b59bd6596d003502141bdaa/target/scripts/startup/daemons-stack.sh#L70
- name: "📝 Copy dovecot.cf"
  ansible.builtin.template:
    src: "dovecot.cf.j2"
    dest: "{{ mailserver_config_dir }}/dovecot.cf"
    mode: "0644"
  become_user: "{{ app_user }}"
  notify: Restart mailserver

- name: "📝 Copy postfix-main.cf"
  ansible.builtin.template:
    src: "postfix-main.cf.j2"
    dest: "{{ mailserver_config_dir }}/postfix-main.cf"
    mode: "0644"
  become_user: "{{ app_user }}"
  notify: Restart mailserver

# https://rspamd.com/doc/configuration/redis.html
- name: "📝 Copy redis.conf"
  ansible.builtin.template:
    src: "redis.conf.j2"
    dest: "{{ mailserver_config_dir }}/redis.conf"
    mode: "0644"
  become_user: "{{ app_user }}"
  notify: Restart mailserver

# # https://docker-mailserver.github.io/docker-mailserver/edge/config/security/rspamd/#dkim-signing
- name: "📝 Copy dkim_signing.conf"
  ansible.builtin.template:
    src: "dkim_signing.conf.j2"
    dest: "{{ mailserver_config_dir }}/dkim_signing.conf"
    mode: "0644"
  become_user: "{{ app_user }}"
  notify: Restart mailserver
  when: mailserver_enable_dkim and mailserver_use_rspamd

- name: "📝 Ensure dkim_signing.conf"
  ansible.builtin.file:
    path: "{{ mailserver_config_dir }}/dkim_signing.conf"
    mode: "0644"
    state: touch

# https://docker-mailserver.github.io/docker-mailserver/edge/config/advanced/user-patches/
- name: "📝 Create {{ mailserver_config_dir }}/user-patches.sh"
  ansible.builtin.template:
    src: "user-patches.sh.j2"
    dest: "{{ mailserver_config_dir }}/user-patches.sh"
    mode: 0644
  become_user: "{{ app_user }}"
  notify: Restart mailserver

- name: "📝 Create {{ mailserver_config_dir }}/whitelist_clients.local for allowed Postgrey hosts"
  ansible.builtin.template:
    src: "postgrey_allowlist.j2"
    dest: "{{ mailserver_config_dir }}/whitelist_clients.local"
    mode: 0644
  become_user: "{{ app_user }}"
  notify: Restart mailserver

- name: '⚙ set mailserver_domains_joined'
  ansible.builtin.set_fact:
    mailserver_domains_joined: "{{ mailserver_domains | join(',') }}"

# https://github.com/docker-mailserver/docker-mailserver#dns---dkim
- include_tasks: roles/container_base/tasks/create_container.yml
  vars:
    app_name: "mailserver-dkim-generator"
    app_command: "setup config dkim keysize {{ dkim_key_size }} domain {{ mailserver_domains_joined }}"
    app_image: "{{ mailserver_image }}"
    app_dir_volumes: "{{ mailserver_dir_volumes }}"
    app_file_mounts: "{{ mailserver_file_mounts }}"
    app_container_user: "{{ mailserver_container_user }}"
    app_env:
      ENABLE_SPAMASSASSIN: '0'
      ENABLE_CLAMAV: '0'
      ENABLE_FAIL2BAN: '0'
      ENABLE_POSTGREY: '0'
      ENABLE_OPENDKIM: "{{ (mailserver_enable_dkim and not mailserver_use_rspamd) | ternary('0', '1') }}"
    app_run_once: true
  when: mailserver_enable_dkim and not mailserver_use_rspamd

- include_tasks: roles/container_base/tasks/create_container.yml
  vars:
    app_name: "mailserver"
    app_image: "{{ mailserver_image }}"
    app_dir_volumes: "{{ mailserver_dir_volumes }}"
    app_file_mounts: "{{ mailserver_file_mounts }}"
    # ports:
      # - "10025:25"   # SMTP  (explicit TLS => STARTTLS)
      # - "10143:143"  # IMAP4 (explicit TLS => STARTTLS)
      # - "10465:465"  # ESMTP (implicit TLS)
      # - "10587:587"  # ESMTP (explicit TLS => STARTTLS)
      # - "10993:993"  # IMAP4 (implicit TLS)
    app_container_user: "{{ mailserver_container_user }}"
    app_service_dependency: "{{ mailserver_use_ldap | ternary('{{ container_service_prefix }}-lldap', False) }}"
    # app_volume_permissions: "{{ mailserver_volume_permissions }}"
    app_env: "{{ mailserver_env }}"
    app_memory: "{{ mailserver_memory }}"
    # https://docker-mailserver.github.io/docker-mailserver/edge/config/advanced/kubernetes/#deployments
    app_cap_add:
      # file permission capabilities
      - CHOWN
      - FOWNER
      - MKNOD
      - SETGID
      - SETUID
      - DAC_OVERRIDE
      # network capabilities
      - NET_ADMIN  # needed for F2B
      - NET_RAW    # needed for F2B
      - NET_BIND_SERVICE
      # miscellaneous  capabilities
      - SYS_CHROOT
      - KILL

- name: "📪 Create email accounts"
  ansible.builtin.command: "podman exec -it mailserver setup email add {{ item.key }} {{ item.value }}"
  environment:
    PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
  loop: "{{ lookup('dict', mailserver_email_accounts) }}"
  register: command_result
  become_user: "{{ app_user }}"
  changed_when: command_result.rc == 0
  failed_when: command_result.rc != 0 and (command_result.stdout is not search('already exists'))
  no_log: true

- name: "📪 Create mail aliases"
  ansible.builtin.command: "podman exec -it mailserver setup alias add {{ item.alias }} {{ item.email }}"
  environment:
    PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
  loop: "{{ mailserver_aliases }}"
  register: command_result
  become_user: "{{ app_user }}"
  changed_when: command_result.rc == 0
  failed_when: command_result.rc != 0 and (command_result.stdout is not search('is already an alias for recipient'))
  # no_log: true

- name: "📁 Create DKIM directories for rspamd"
  ansible.builtin.command: "podman exec -t mailserver mkdir -m 774 -p {{ mailserver_internal_rspamd_dkim_key_dir }}/{{ item }}/keys"
  # creates: "{{ mailserver_external_rspamd_dkim_key_dir }}/{{ item }}/keys"
  environment:
    PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
    XDG_RUNTIME_DIR: "{{ xdg_runtime_dir }}"
  become_user: "{{ app_user }}"
  with_items: "{{ mailserver_domains }}"
  when: mailserver_enable_dkim and mailserver_use_rspamd
  notify: Restart mailserver

- name: "🔑 Create DKIM keys for rspamd"
  ansible.builtin.shell: >-
    podman exec -t mailserver /bin/bash -c "rspamadm dkim_keygen --selector {{ mailserver_rspamd_dkim_selector }} \
                           --bits {{ dkim_key_size }} \
                           --domain {{ item }} \
                           --privkey {{ mailserver_internal_rspamd_dkim_key_dir }}/{{ item }}/keys/{{ mailserver_rspamd_dkim_selector }}.private \
                           > {{ mailserver_internal_rspamd_dkim_key_dir }}/{{ item }}/keys/{{ mailserver_rspamd_dkim_selector }}.txt"
  # creates: "{{ mailserver_external_rspamd_dkim_key_dir }}/{{ item }}/keys/{{ mailserver_rspamd_dkim_selector }}.txt"
  with_items: "{{ mailserver_domains }}"
  environment:
    PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
    XDG_RUNTIME_DIR: "{{ xdg_runtime_dir }}"
  become_user: "{{ app_user }}"
  when: mailserver_enable_dkim and mailserver_use_rspamd
  notify: Restart mailserver

- name: "🔑 Set DKIM owner to _rspamd"
  ansible.builtin.shell: >-
    podman exec mailserver chown _rspamd:_rspamd -R {{ mailserver_internal_rspamd_dkim_key_dir }}
  environment:
    PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
    XDG_RUNTIME_DIR: "{{ xdg_runtime_dir }}"
  become_user: "{{ app_user }}"
  when: mailserver_enable_dkim and mailserver_use_rspamd
  notify: Restart mailserver

# - name: "🔑 Set make DKIM keys accessible to app user"
#   ansible.builtin.shell: >-
#     chmod 0766 {{ mailserver_external_rspamd_dkim_key_dir }}/{{ item }}/keys/
#   with_items: "{{ mailserver_domains }}"
#   environment:
#     PATH: "{{ homebrew_bin_dir }}:{{ ansible_env.PATH }}"
#     XDG_RUNTIME_DIR: "{{ xdg_runtime_dir }}"
#   become_user: "{{ app_user }}"
#   when: mailserver_enable_dkim and mailserver_use_rspamd
#   notify: Restart mailserver
