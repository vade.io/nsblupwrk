---
# https://docs.ansible.com/ansible/2.9/modules/ufw_module.html
# https://github.com/Oefenweb/ansible-ufw/issues/25

- name: "📥 Install Uncomplicated Firewall (ufw)"
  ansible.builtin.apt:
    name:
      - "ufw"
  become: true

- name: '⚙ set fact all_network_interfaces'
  ansible.builtin.set_fact:
    all_network_interfaces: "{{ all_network_interfaces | default([]) + [item] }}"
  with_items: "{{ ansible_interfaces }}"
  when: item != "lo"

- name: "🐡 Enable ufw access for OpenSSH"
  community.general.ufw:
    rule: allow
    name: OpenSSH

- name: "🧱 Uncomplicated Firewall (ufw)"
  community.general.ufw:
    state: enabled

- name: "⤴ Enable forward policy"
  ansible.builtin.lineinfile:
    path: /etc/default/ufw
    regexp: '^DEFAULT_FORWARD_POLICY'
    line: 'DEFAULT_FORWARD_POLICY="ACCEPT"'
  become: true
  notify:
    - restart ufw

- name: "⤴ Enable ip forward"
  ansible.builtin.lineinfile:
    path: /etc/ufw/sysctl.conf
    regexp: '{{ item | regex_escape() }}'
    line: "{{ item }}=1"
  become: true
  with_items: ["net/ipv4/ip_forward"]
  notify:
    - restart ufw

- name: "🧭 Enable ip local net route"
  ansible.builtin.lineinfile:
    path: "/etc/ufw/sysctl.conf"
    regexp: '{{ "net/ipv4/conf/" + item + "/route_localnet" | regex_escape() }}'
    line: "net/ipv4/conf/{{ item }}/route_localnet=1"
  become: true
  with_items: "{{ all_network_interfaces }}"
  notify:
    - restart ufw

- name: '⚙ set fact ufw_port_forward_settings'
  ansible.builtin.set_fact:
    ufw_port_forward_settings: "{{ ufw_port_forward_settings | default([]) + [{'dest_port': (item | split(':'))[0], 'src_port': ((item | split(':'))[1] | split('/'))[0], 'protocol': ((item | split(':'))[1] | split('/'))[1] | default('tcp') }] }}"
  with_items: "{{ published_ports }}"
  when: ((item | split(':'))[1] | split('/'))[0] | int < 1024
  no_log: true

# it needs to have the ports public to work
- name: "✔ Allow source ports"
  community.general.ufw:
    rule: allow
    # don't set `dest` here unless you know the exact IP address of the destination
    # You can find out by looking into the ufw log
    port: "{{ item.src_port }}"
  with_items: "{{ ufw_port_forward_settings }}"
  become: true
  notify:
    - restart ufw

- name: "✔ Allow traffic to dest ports"
  community.general.ufw:
    rule: allow
    # don't set `dest` here unless you know the exact IP address of the destination
    # You can find out by looking into the ufw log
    direction: in
    port: "{{ item.dest_port }}"
  with_items: "{{ ufw_port_forward_settings }}"
  become: true
  notify:
    - restart ufw

- name: "🔃 Insert nat rules (IPv4) into UFW's before.rules"
  ansible.builtin.blockinfile:
    path: "/etc/ufw/before.rules"
    insertbefore: BOF
    block: |
      *nat
      :PREROUTING ACCEPT [0:0]
      # :POSTROUTING ACCEPT [0:0]
      {% for item in ufw_port_forward_settings %}
      # Redirect {{ item.protocol }}-traffic from port '{{ item.src_port }}' to '{{ item.dest_port }}'
      -A PREROUTING -p {{ item.protocol }} --dport {{ item.src_port }} -j REDIRECT --to-port {{ item.dest_port }}
      {% endfor %}
      COMMIT
    state: present
  become: true
  notify:
    - restart ufw
