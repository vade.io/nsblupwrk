# podman_bunkerweb

This role will install a bunkerweb container on a podman enabled server.
The configuration needed for bunkerweb is added using a translation of a docker-compose services list.

## Requirements

Podman needs to be installed on the system

## Role Variables

The role is using a ```services``` list containing dictionary object as shown below

```yaml
services:
  - app_name: bunkerweb
    image: bunkerity/bunkerweb:1.4.8
    ports:
      - 80:8080
      - 443:8443
    environment:
      SERVER_NAME: 'www.example.com'
      USE_REVERSE_PROXY: 'yes'
      REVERSE_PROXY_URL: '/'
      REVERSE_PROXY_HOST: 'http://myapp1:80'
      MULTISITE: 'no'
      DNS_RESOLVERS: '10.89.0.1'
      USE_REAL_IP: 'yes'
      REAL_IP_FROM: '1.2.3.0/24 192.168.0.0/24'
      REAL_IP_HEADER: 'X-Forwarded-For'
      AUTO_REDIRECT_HTTP_TO_HTTPS: 'yes'
      HTTP2: 'yes'
      AUTO_LETS_ENCRYPT: 'yes'
      EMAIL_LETS_ENCRYPT: 'contact@example.org'
      USE_LETS_ENCRYPT_STAGING: 'yes'
      USE_MODSECURITY: 'yes'
      USE_MODSECURITY_CRS: 'yes'
      USE_BAD_BEHAVIOR: 'yes'
      USE_ANTIBOT: 'captcha'
      WHITELIST_IP: '20.191.45.212 40.88.21.235 40.76.173.151 40.76.163.7 20.185.79.47 52.142.26.175 20.185.79.15 52.142.24.149 40.76.162.208 40.76.163.23 40.76.162.191 40.76.162.247 54.208.102.37 107.21.1.8'
      WHITELIST_RDNS: .google.com .googlebot.com .yandex.ru .yandex.net .yandex.com .search.msn.com .baidu.com .baidu.jp .crawl.yahoo.net .fwd.linkedin.com .twitter.com .twttr.com .discord.com'
      USE_BUNKERNET: 'false'
      USE_DNSBL: 'yes'
      DNSBL_LIST: 'bl.blocklist.de problems.dnsbl.sorbs.net sbl.spamhaus.org xbl.spamhaus.org'
      USE_API: 'yes'
      USE_BROTLI: 'yes'
      USE_GZIP: 'yes'
      USE_UI: 'no'
      MULTISITE: 'yes'
      SERVE_FILES: 'no'
      USE_CLIENT_CACHE: 'yes'
      USE_LIMIT_REQ: 'yes'
      LIMIT_REQ_RATE: '40r/s'
      BLACKLIST_COUNTRY: 'cn'
      BAD_BEHAVIOR_STATUS_CODES: '400 401 403 404 405 444'
      BAD_BEHAVIOR_THRESHOLD: '25'
    networks:
      - bw-bunker
```

## Dependencies

```podman_systemd``` role is needed to configure the container in podman.

## Example Playbook

The example below will install bunkerweb as container running in podman, controlled by systemd:

```yaml
- hosts: all
  become: true

  tasks:
  - name: install bunkerweb
    ansible.builtin.include_role:
      name: podman_bunkerweb
    vars:
      services:
      - app_name: bunkerweb
        image: bunkerity/bunkerweb:1.4.8
        ports:
          - 80:8080
        environment:
          SERVER_NAME: 'www.example.com'
          USE_REVERSE_PROXY: 'yes'
          REVERSE_PROXY_URL: '/'
          REVERSE_PROXY_HOST: 'http://grafana:3000'
          MULTISITE: 'no'
          DNS_RESOLVERS: '10.89.0.1'
        networks:
          - bunker
```
